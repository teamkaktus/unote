'use strict';

var crypto = require('crypto');

module.exports = function (User) {
  User.register = function (data, cb) {
    data.password = crypto.createHash('md5').update(data.password).digest("hex") || '';
    User.findOne({
      where: {
        email: data.email
      }
    }, function (err, user) {
      if (err) {
        cb(null, {
          status: false,
          message: 'Db error',
          field: 'db'
        });
      } else {
        if (!user) {
          User.create(data, function (err, recordId) {
            if (err) {
              cb(null, {
                status: false,
                message: 'Db error',
                field: 'db'
              });
            } else {
              cb(null, {
                status: true,
                message: data
              });
            }
          })
        } else {
          if (user.email.toUpperCase() == data.email.toUpperCase()) {
            cb(null, {
              status: false,
              message: 'This Email is already in use by another member. Please select another Email.',
              field: 'email'
            });
          }
        }
      }
    });
  }

  User.remoteMethod('register', {
    accepts: {
      arg: 'data',
      type: 'user',
      http: {
        source: 'body'
      }
    },
    http: {
      verb: 'post'
    },
    description: "Register user methods",
    returns: [{
        arg: 'status',
        type: 'boolean'
      },
      {
        arg: 'message',
        type: 'string'
      },
      {
        arg: 'field',
        type: 'string'
      }
    ]
  });

  User.login = function (email, password, cb) {
    User.findOne({
      where: {
        email: email
      }
    }, function (err, user) {
      if (err) {
        cb(null, {
          status: false,
          message: 'Db error',
          field: 'db'
        });
      } else {
        if (user) {
          var passwordHash = crypto.createHash('md5').update(password).digest("hex") || '';
          if (passwordHash == user.password) {
            cb(null, {
              status: true,
              id: user.id,
              fio: user.fio,
              email: user.email
            });
          } else {
            cb(null, {
              status: false,
              message: 'Invalid password',
              field: "password"
            });
          }
        } else {
          cb(null, {
            status: false,
            message: 'User not found with email: ' + email,
            field: "email"
          });
        }
      }
    });
  }

  User.remoteMethod('login', {
    accepts: [{
        arg: 'email',
        type: 'string'
      },
      {
        arg: 'password',
        type: 'string'
      }
    ],
    http: {
      verb: 'get'
    },
    description: "Login user methods",
    returns: [{
        arg: 'status',
        type: 'boolean'
      },
      {
        arg: 'message',
        type: 'string'
      },
      {
        arg: 'field',
        type: 'string'
      }
    ]
  });

};
