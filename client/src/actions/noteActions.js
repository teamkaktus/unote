import axios from '../axios-config';

export function createNote(data) {
  const request = axios.post('notes',data);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'CREATE_NOTE', payload: data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_NOTE', err });
    });
  };
}

export function getNotes(id) {
  const request = axios.get('notes/');
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'GET_NOTES', payload: data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_NOTE', err });
    });
  };
}


export function updateNote(data) {
  const request = axios.put('notes',data);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'UPDATE_NOTE', payload: data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_NOTE', err });
    });
  };
}

export function deleteNote(id) {
  const request = axios.delete('notes/'+id);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
      dispatch({ type: 'DELETE_NOTE', id });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_NOTE', err });
    });
  };
}

export function getUsers() {
  const request = axios.get('users');
  return (dispatch) => {
    request.then(({ data }) => {
      dispatch({ type: 'USERS_NOTE', data });
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_NOTE', err });
    });
  };
}