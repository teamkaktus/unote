import axios from '../axios-config';
import { browserHistory } from 'react-router';

export function Login(data) {
  const request = axios.get('users/login',{
    params: data
  });
  return (dispatch) => {
    request.then(({ data }) => {
     
      dispatch({ type: 'AUTH_LOGIN', payload: data.status });
      browserHistory.push('/subjects');
    }).catch((err) => {
      dispatch({ type: 'ERROR_AUTH', err });
    });
  };
}

export function Register(data) {
  console.log('Register', data)
  const request = axios.post('users/register',data);
  return (dispatch) => {
    request.then(({ data }) => {
        console.log('create note', data)
     // dispatch({ type: 'AUTH_LOGIN', payload: data.status });
     alert('Ви успішно зареєструвались! Будласка виконайте вхід!');
      browserHistory.push('/login');
    }).catch((err) => {
        console.log('create note err', err)
      dispatch({ type: 'ERROR_AUTH', err });
    });
  };
}

export function Logout() {
  return (dispatch) => {
      localStorage.clear();
      dispatch({ type: 'AUTH_LOGOUT' });
      browserHistory.push('/login');
  };
}

