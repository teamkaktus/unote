import React from 'react';
import Note from './note/note.js';
import CreateNote from './note/createNote.js';
import { connect } from 'react-redux';
import { getNotes } from '../actions/noteActions';
import { Link } from 'react-router';
// import Header from '../layout/Header';

class NotePage extends React.Component {
    constructor(props) {
        super(props);
        this.props.getNotes(this.props.params.id);
    }

    render() {
        console.log('notes', this.props.notes)
        return (
            <div className="row">
                <CreateNote subject_id={this.props.params.id}/>
                <Link to="/subjects"><buttun className="button_return_subject btn">Повернутись до тем</buttun></Link>
                {this.props.notes.map(note => <Note subject_id={this.props.params.id} data={note}/>)}
            </div>
        );
    }
}

export default connect((state) => {return state.note}, {getNotes})(NotePage);