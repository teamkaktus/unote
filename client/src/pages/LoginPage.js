import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Login } from '../actions/authActions';
import { browserHistory } from 'react-router';
import {isLogin} from '../lib/helper';
// import Header from '../layout/Header';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
        this.login = this.login.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    login(e){
        e.preventDefault();
        this.props.Login(this.state);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    componentWillMount() {
        if (isLogin()){
            browserHistory.push('/subjects');
        }
    }
   
    render() {
        console.log('store', localStorage.getItem('user'));
        return (
            <div className="row">
                <form className="form-signin" onSubmit={this.login}>
                    <h2 className="form-signin-heading">Авторизація</h2>
                    <label for="inputEmail" className="sr-only">Email address</label>
                    <input type="email" onChange={this.handleChange} name="email" value={this.state.email} id="inputEmail" className="form-control input_emai_style" placeholder="Email address" required="" autofocus=""/>
                    <label for="inputPassword" className="sr-only">Password</label>
                    <input type="password" onChange={this.handleChange} name="password" value={this.state.password} id="inputPassword"  className="form-control input_password_style" placeholder="Password" required=""/>
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Увійти</button>
                    <Link to="/register" className="btn btn-lg btn-primary btn-block">Зареєструватися</Link>
                </form>
            </div>
        );
    }
}

export default connect((state) => {return state.auth}, {Login})(LoginPage);