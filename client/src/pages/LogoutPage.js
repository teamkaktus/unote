import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Logout } from '../actions/authActions';
import { browserHistory } from 'react-router';
import {isLogin} from '../lib/helper';
// import Header from '../layout/Header';

class LogoutPage extends React.Component {
    constructor(props) {
        super(props);
        this.props.Logout();
    }

    render() {
        return null;
    }
}

export default connect((state) => {return state.auth}, {Logout})(LogoutPage);