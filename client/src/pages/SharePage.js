import React from 'react';
import Note from './note/note.js';
import CreateNote from './note/createNote.js';
import { connect } from 'react-redux';
import { getNotes } from '../actions/noteActions';
import { Link } from 'react-router';
// import Header from '../layout/Header';

class NotePage extends React.Component {
    constructor(props) {
        super(props);
        this.props.getNotes(this.props.params.id);
    }

    render() {
        console.log('notes', this.props.notes)
        return (
            <div className="row container_sharePage">
                {this.props.notes.map(note => <Note type="share" subject_id={this.props.params.id} data={note}/>)}
            </div>
        );
    }
}

export default connect((state) => {return state.note}, {getNotes})(NotePage);