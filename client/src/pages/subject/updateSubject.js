import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { updateSubject } from '../../actions/subjectActions';
// import Header from '../layout/Header';

class UpdateSubject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            title: ''
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updateSubject = this.updateSubject.bind(this);
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true, title: this.props.data.title });
    }

     handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    updateSubject(){
        this.props.updateSubject({id: this.props.data.id, title: this.state.title, user_id: this.props.data.user_id});
        this.setState({
            showModal: false,
            title: ''})
    }

    render() {
        return (
            <div className="container_button_update_subject"> 
                <button className="button_update_subject btn" onClick={this.open}>Редагувати</button>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Редагувати тему</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                        <div className="form-group">
                            <label>Тема</label>
                            <input type="text" className="form-control" name="title" value={this.state.title} onChange={this.handleChange}/>
                        </div>
                    
                </Modal.Body>
                <Modal.Footer>
                    <Button className="button_modal_save btn" onClick={this.updateSubject}>Зберегти</Button>
                    <Button className="button_modal_close btn" onClick={this.close}>Закрити</Button>
                </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default connect((state) => {return state.subject}, { updateSubject })(UpdateSubject);