import React from 'react';
import UpdateSubject from './updateSubject';
import { connect } from 'react-redux';
import { deleteSubject } from '../../actions/subjectActions';
import { browserHistory } from 'react-router'
// import Header from '../layout/Header';

class Subject extends React.Component {
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
        this.view = this.view.bind(this);
    }

    delete(){
        this.props.deleteSubject(this.props.data.id);
    }

    view(){
        browserHistory.push('/subjects/'+this.props.data.id);
    }

    render() {
        if (this.props.data.user_id == localStorage.getItem('userId'))
            return (
                <div className="panel panel-info col-lg-4 container_subject"> 
                    <div className="panel-heading container_title_subject">
                        <span className="panel-title text_title_subject">{this.props.data.title}</span>
                    </div>
                    <div className="buttont_subject">
                        <UpdateSubject data={this.props.data}/>
                        <button className="button_delete_subject btn" onClick={this.delete}>Видалити</button>
                        <button className="button_view_subject btn" onClick={this.view}>Переглянути</button>
                    </div>
                    
                </div>
            )
        else
            return null;
    }
}

export default connect((state) => {return state.subject}, { deleteSubject })(Subject);