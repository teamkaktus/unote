import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { updateNote } from '../../actions/noteActions';
// import Header from '../layout/Header';

class UpdateNote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            name: '',
            description: ''
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updateNote = this.updateNote.bind(this);
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true, name: this.props.data.name, description: this.props.data.description });
    }

     handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    updateNote(){
        this.props.updateNote({id: this.props.data.id, name: this.state.name, description: this.state.description, subject_id: this.props.subject_id});
        this.setState({
            showModal: false,
            name: '',
            description: ''})
    }

    render() {
        return (
            <div className="container_button_update_subject"> 
                <button className="button_update_subject btn" onClick={this.open}>Редагувати</button>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Редвгувати замітку</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                        <div className="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label for="exampleInputPassword1">Disc</label>
                            <textarea className="form-control" name="description" value={this.state.description} onChange={this.handleChange}/>
                        </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="button_modal_save btn" onClick={this.updateNote}>Зберегти</Button>
                    <Button className="button_modal_close btn" onClick={this.close}>Закрити</Button>
                </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default connect((state) => {return state.note}, { updateNote })(UpdateNote);