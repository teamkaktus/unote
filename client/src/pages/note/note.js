import React from 'react';
import UpdateNote from './updateNote';
import ShareNote from './shareNote';
import { connect } from 'react-redux';
import { deleteNote } from '../../actions/noteActions';
// import Header from '../layout/Header';

class Note extends React.Component {
    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }

    delete(){
        this.props.deleteNote(this.props.data.id);
    }

    render() {
        console.log('data', this.props.data);
        if (this.props.type == "share")
         return (
                <div className="panel panel-info col-lg-4 container_note"> 
                    <div className="panel-heading title_node">
                        <span className="panel-title">{this.props.data.name}</span>
                    </div>
                    <div className="panel-body description_node"> {this.props.data.description} </div>
                </div>
            )
        else if (this.props.data.subject_id == this.props.subject_id)
            return (
                <div className="panel panel-info col-lg-4 container_note"> 
                    <div className="panel-heading title_node">
                        <span className="panel-title">{this.props.data.name}</span>
                    </div>
                    <div className="panel-body description_node"> {this.props.data.description} </div>
                    <div className="container_button_note">
                        <UpdateNote data={this.props.data} subject_id={this.props.subject_id}/>
                        <button className="button_delete_subject btn" onClick={this.delete}>Видалити</button>
                        <ShareNote data={this.props.data}/>
                    </div>
                </div>
            )
        else
          return null;
    }
}

export default connect((state) => {return state.note}, { deleteNote })(Note);