import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { getUsers } from '../../actions/noteActions';
import { addShare } from '../../actions/shareActions'
// import Header from '../layout/Header';

class ShareNote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            user_id: '',
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.addShare = this.addShare.bind(this);
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.props.getUsers();
        this.setState({ showModal: true});
    }

     handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    addShare(){
        this.props.addShare({note_id: this.props.data.id, user_id: this.state.user_id});
        this.setState({
            showModal: false,
            user_id: ''});
    }

    render() {
        return (
            <div className="container_button_update_subject"> 
                <button className="button_update_subject btn" onClick={this.open}>Доступ</button>

                <Modal show={this.state.showModal} onHide={this.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Надати доступ</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                        <div className="form-group">
                            <label>Користувач</label>
                            <select className="select_shareNote_style" value={this.state.user_id} name="user_id" onChange={this.handleChange}>
                                {this.props.users.map(user => <option value={user.id}>{user.fio}</option>)}
                            </select>
                        </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="button_modal_save btn" onClick={this.addShare}>Надати</Button>
                    <Button className="button_modal_close btn" onClick={this.close}>Закрити</Button>
                </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default connect((state) => {return state.note}, { addShare, getUsers })(ShareNote);