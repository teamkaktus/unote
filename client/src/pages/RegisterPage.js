import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Register } from '../actions/authActions';
import { browserHistory } from 'react-router';
import {isLogin} from '../lib/helper';
// import Header from '../layout/Header';

class RegisterPage extends React.Component {
   constructor(props) {
        super(props);
        this.state = {
            fio: '',
            email: '',
            password: ''
        }
        this.register = this.register.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    register(e){
         e.preventDefault();
        this.props.Register(this.state);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    componentWillMount() {
        if (isLogin()){
            browserHistory.push('/subjects');
        }
    }
   
    render() {
        return (
            <div className="row">
                <form className="form-signin" onSubmit={this.register}>
                    <h2 className="form-signin-heading">Реєстрація</h2>
                    <label for="inputName" className="sr-only">Name</label>
                    <input type="text" onChange={this.handleChange} id="inputName" name="fio" value={this.state.fio} className="form-control input_name_style" placeholder="Name" required="" autofocus=""/>
                    <label for="inputEmail" className="sr-only">Email address</label>
                    <input type="email" onChange={this.handleChange} name="email" value={this.state.email} id="inputEmail" className="form-control input_emai_style" placeholder="Email address" required="" autofocus=""/>
                    <label for="inputPassword" className="sr-only">Password</label>
                    <input type="password" onChange={this.handleChange} name="password" value={this.state.pasword} id="inputPassword" className="form-control input_password_style" placeholder="Password" required=""/>
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Зареєструватися</button>
                    <Link to="/login" className="btn btn-lg btn-primary btn-block">Вхід</Link>
                </form>
            </div>
        );
    }
}

export default connect((state) => {return state.auth}, {Register})(RegisterPage);