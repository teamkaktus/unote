import { combineReducers } from 'redux';
import noteReducer from  './reducers/noteReducer';
import subjectReducer from  './reducers/subjectReducer';
import authReducer from  './reducers/authReducer';

export default combineReducers ({
    note: noteReducer,
    subject: subjectReducer,
    auth: authReducer
});