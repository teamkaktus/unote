import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import NotePage from './pages/NotePage';
import SubjectPage from './pages/SubjectPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import LogoutPage from './pages/LogoutPage';
import SharePage from './pages/SharePage';

import {isLogin} from './lib/helper';

function checkLoginChange(prevState, nextState, replace) {
  if (!isLogin()){
    console.log('checkLoginChange',nextState)
    switch (nextState.location.pathname) {
        case '/login':
            break;
        case '/register':
            break;
        default:
           replace('/login')
    }      
  }
}

function checkLoginEnter(nextState, replace) {
  if (!isLogin()){
    console.log('checkLoginEnter',nextState)
  switch (nextState.location.pathname) {
        case '/login':
            break;
        case '/register':
            break;
        default:
           replace('/login')
    }   
  }
}

export default (
<Route path="/" component={App} onEnter={checkLoginEnter} onChange={checkLoginChange}>
    <IndexRoute component={LoginPage} />
    <Route path="subjects" component={SubjectPage}/>
    <Route path="subjects/:id" component={NotePage}/>
    <Route path="share" component={SharePage}/>
    <Route path="login" component={LoginPage}/>
    <Route path="logout" component={LogoutPage}/>
    <Route path="register" component={RegisterPage}/>
</Route>
);