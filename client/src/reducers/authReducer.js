export default (state = {}, action = {}) => {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case 'AUTH_LOGIN':
            localStorage.setItem('user',  action.payload.fio);
            localStorage.setItem('userId',  action.payload.id);
            return Object.assign({}, state, {
                user: action.payload,
            });
        case 'ERROR_AUTH':
            return Object.assign({}, state, {
                error: action.payload,
            });

        case 'AUTH_LOGOUT':
            return Object.assign({}, state, {
                user: {status: false},
            });
        default:
            return newState;
    }
};