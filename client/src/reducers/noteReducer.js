export default (state = {}, action = {}) => {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case 'GET_NOTES':
            console.log('GET_USERS', action.payload);
            return Object.assign({}, state, {
                notes: action.payload,
            });
        case 'USERS_NOTE':
            return Object.assign({}, state, {
                users: action.data,
            });
        case 'CREATE_NOTE':
            console.log('CREATE_NOTE', action.payload);
            return Object.assign({}, state, {
                notes: [...state.notes, action.payload]
            });
        case 'UPDATE_NOTE':
            let notesUpdate = [];
            state.notes.map(note => {
                if (note.id == action.payload.id){
                    notesUpdate.push(action.payload)
                } else {
                    notesUpdate.push(note);
                }
            })
            return Object.assign({}, state, {
                notes: notesUpdate
            });
        case 'DELETE_NOTE':
            let notesDelete = [];
            state.notes.map(note => {
                if (note.id != action.id){
                    notesDelete.push(note);
                }
            })
            return Object.assign({}, state, {
                notes: notesDelete
            });
        case 'ERROR_USERS':
            console.log('error',action.err);
            return newState;
        default:
            return newState;
    }
};