var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.MySql;

ds.automigrate('user', function(err) {
  if (err) throw err;
});

ds.automigrate('subject_note', function(err) {
  if (err) throw err;
});

ds.automigrate('note', function(err) {
  if (err) throw err;
});

ds.automigrate('share', function(err) {
  if (err) throw err;
});

ds.automigrate('follow', function(err) {
  if (err) throw err;
});